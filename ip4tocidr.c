#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <string.h>

typedef struct iprange
{
    uint32_t init;
    uint32_t last;
} iprange_t;

typedef struct ipsubnet
{
    uint32_t addr;
    uint8_t size;
} ipsubnet_t;

static inline void iprange_make(uint32_t init, uint32_t last,
                                iprange_t* iprange)
{
    iprange->init = init;
    iprange->last = last;
}

static inline void iprange_make_empty(iprange_t* iprange)
{
    iprange->init = 0;
    iprange->last = 0;
}

static inline void ipsubnet_make(uint32_t addr, uint8_t size,
                                 ipsubnet_t* ipsubn)
{
    ipsubn->addr = addr;
    ipsubn->size = size;
}

static inline void ipsubnet_make_empty(ipsubnet_t* ipsubn)
{
    ipsubn->addr = 0;
    ipsubn->size = 32;
}

static inline void ipsubnet_make_single(uint32_t addr, ipsubnet_t* ipsubn)
{
    ipsubn->addr = addr;
    ipsubn->size = 32;
}

static inline int ipaddr_parse(const char* str, uint32_t* addr)
{
    struct in_addr in;
    if(inet_aton(str, &in))
    {
        *addr = ntohl(in.s_addr);
        return 1;
    }
    return 0;
}

static char* ipaddr_to_str(uint32_t addr)
{
    struct in_addr in;
    in.s_addr = htonl(addr);
    return inet_ntoa(in);
}

static inline int iprange_cmp(iprange_t* lhs, iprange_t* rhs)
{
    if(lhs->init == rhs->init && lhs->last == rhs->last) {
        return 0;
    } else if(lhs->init < rhs->init ||
              lhs->init == rhs->init && lhs->last < rhs->last) {
        return -1;
    } else {
        return 1;
    }
}

static inline int ipsubnet_cmp(ipsubnet_t* lhs, ipsubnet_t* rhs)
{
    if(lhs->addr == rhs->addr && lhs->size == rhs->size) {
        return 0;
    } else if(lhs->addr < rhs->addr ||
              lhs->addr == rhs->addr && lhs->size < rhs->size) {
        return -1;
    } else {
        return 1;
    }
}

static inline void iprange_swap(iprange_t* rng)
{
    uint32_t tmp = rng->init;
    rng->init = rng->last;
    rng->last = rng->init;
}

static inline void iprange_norm(iprange_t* rng)
{
    if(rng->init > rng->last)
        iprange_swap(rng);
}

static inline uint32_t popcnt(uint32_t x)
{
    x -= (x >> 1) & 0x55555555;
    x = ((x & 0x33333333) + ((x >> 2) & 0x33333333)) | 0;
    x = ((((x + (x >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24) & 0xFF;
    return x;
}

static inline uint32_t intlog2(uint64_t x)
{
    uint32_t i;
    for(i = 0; x >>= 1; ++i);
    return i;
}

static size_t range_to_subnets(const iprange_t* rng, ipsubnet_t* ipsubns)
{
    size_t num_subns = 0;
    uint64_t init = rng->init;
    uint64_t last = rng->last;
    while(last >= init)
    {
        uint64_t power = 32u - intlog2(last - init + 1);
        uint64_t magic = popcnt(-(init & (-init)));
        uint64_t mask_size = (magic < power ? power : magic);
        if(ipsubns != NULL) {
            ipsubnet_make(init, mask_size, ipsubns + num_subns);
        }
        ++num_subns;
        init += ((uint64_t)1 << (32u - mask_size));
    }
    return num_subns;
}

static void subnet_to_range(const ipsubnet_t* ipsubn, iprange_t* rng)
{
    iprange_make(ipsubn->addr, ipsubn->addr + (1u << (32u - ipsubn->size)),
                 rng);
}

int main(int argc, char* argv[])
{
    uint32_t init, last;

    if (argc < 3) {
        printf("Usage: ip4tocidr <start-ip> <end-ip>\n\n");
        printf("\te.g. ip4tocidr 127.0.0.1 127.0.0.254\n");
        return -1;
    }

    ipaddr_parse(argv[1], &init);
    ipaddr_parse(argv[2], &last);

    iprange_t range;
    iprange_make(init, last, &range);

    ipsubnet_t subn[255];
    size_t num_subn = range_to_subnets(&range, subn);
    size_t i;
    for(i = 0; i < num_subn; ++i) {
        char original[255] = {0};
        strcpy(original, ipaddr_to_str(subn[i].addr));

        char first[255] = {0};
        strcpy(first, ipaddr_to_str(subn[i].addr));

        char second[255] = {0};
        strcpy(second, ipaddr_to_str(subn[i].addr));

        iprange_t tmp;
        subnet_to_range(subn + i, &tmp);
        printf("%s/%d\t\t\t\t -- %s-%s\n", original, subn[i].size,
               first, second);
    }
    return 0;
}
